#ifndef UNTITLEDCLASSES_REALSTACK_H
#define UNTITLEDCLASSES_REALSTACK_H

#include <iostream>
#include <list>

#include "Stack.h"

class RealStack: IntegerStack {
private:
    std::list<int> stack = {};
    int limit = 42;
public:
    RealStack(std::list<int> stack);
    RealStack();

    virtual bool isEmpty();
    virtual int size();
    virtual void push(int number);
    virtual int pop();

  const int &getLimit() const { return this->limit; };
  void setLimit(const int &newlimit) { this->limit = newlimit; };
};

#endif // UNTITLEDCLASSES_REALSTACK_H