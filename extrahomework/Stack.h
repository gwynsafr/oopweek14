#ifndef UNTITLEDCLASSES_INTEGERSTACK_H
#define UNTITLEDCLASSES_INTEGERSTACK_H

#include <iostream>

class IntegerStack {
public:
  virtual bool isEmpty() = 0;
  virtual int size() = 0;
  virtual void push(int number) = 0;
  virtual int pop() = 0;
};

#endif // UNTITLEDCLASSES_INTEGERSTACK_H