#ifndef UNTITLEDCLASSES_ContractReport_H
#define UNTITLEDCLASSES_ContractReport_H

#include <iostream>
#include <string>
#include <utility>

#include "Datacollection.h"
#include "SimpleReport.h"

class ContractReport: SimpleReport {
public:
  void print(std::list<Contract>);
};

#endif // UNTITLEDCLASSES_ContractReport_H