#ifndef UNTITLEDCLASSES_WORKERREPORT_H
#define UNTITLEDCLASSES_WORKERREPORT_H

#include <iostream>
#include <string>
#include <utility>

#include "Datacollection.h"
#include "SimpleReport.h"

class WorkerReport: SimpleReport {
public:
  void print(std::list<Contract>);
};

#endif // UNTITLEDCLASSES_WORKERREPORT_H