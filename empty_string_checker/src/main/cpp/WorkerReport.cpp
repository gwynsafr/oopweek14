#include "WorkerReport.h"

void WorkerReport::print(std::list<Contract> contracts) {
    for (Contract contract : contracts) {
        std::cout << contract << std::endl;
    }
}